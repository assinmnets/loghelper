package com.shubh.loghelper.mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Publisher {

    private MqttClient client;

    private final String username = "dgavalpb";
    private final String password = "Q6ipars9ERGf";
    private final String serverUri = "tcp://m10.cloudmqtt.com:15545";

    private static final Publisher sPUBLISHER = new Publisher();
    private boolean isInitialised;

    public static Publisher getInstance() throws MqttException {
        if (!sPUBLISHER.isInitialised) {
            sPUBLISHER.init();
        }
        return sPUBLISHER;
    }

    private void init() throws MqttException {
        client = new MqttClient(serverUri, MqttClient.generateClientId());
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);
        mqttConnectOptions.setUserName(username);
        mqttConnectOptions.setPassword(password.toCharArray());
        client.connect(mqttConnectOptions);
        isInitialised = true;
    }

    public void publish(String messageString) throws MqttException {

        System.out.println("== START PUBLISHER ==");

        MqttMessage message = new MqttMessage();
        message.setPayload(messageString.getBytes());
        client.publish("logs/filedata", message);

        System.out.println("\tMessage '" + messageString + "' logs/filedata'");

        System.out.println("== END PUBLISHER ==");

    }
}