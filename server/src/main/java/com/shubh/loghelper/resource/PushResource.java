package com.shubh.loghelper.resource;

import com.shubh.loghelper.pushservice.FcmClient;
import com.shubh.loghelper.mqtt.Publisher;
import com.shubh.loghelper.pushservice.EntityMessage;
import com.shubh.loghelper.pushservice.FcmResponse;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Service
@Path("/push")
@Produces(MediaType.APPLICATION_JSON)
public class PushResource {

    @Autowired
    private PushRepository repository;

    @Context
    private Response response;

    @Context
    private UriInfo uriInfo;


    @GET
    @Path("/registration/{registrationToken}")
    public Response registerToken(@PathParam("registrationToken") String registrationToken) {
        String token = repository.registrationToken(registrationToken);
        return Response.ok(token).build();
    }

    @GET
    @Path("/message/send/{title}")
    public Response sendPushNotification(@PathParam("title") String title) {

        FcmClient client = new FcmClient();
        EntityMessage msg = new EntityMessage();
        repository.getRegistrationTokens().values().forEach(msg::addRegistrationToken);

        msg.putStringData("title", title);
        msg.putStringData("myKey2", "myValue2");

        FcmResponse res = client.pushToEntities(msg);
        return Response.ok(res).build();
    }

    @GET
    @Path("/logs/send/{logs}")
    public Response sendLogs(@PathParam("logs") String logs) {
        try {
            Publisher.getInstance().publish(logs);
            return Response.ok("Sent").build();
        } catch (MqttException e) {
            return Response.ok(e.getMessage()).build();
        }
    }

    private Response notFound() {
        return Response.status(Response.Status.NOT_FOUND).build();
    }

    private Response badRequest() {
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
