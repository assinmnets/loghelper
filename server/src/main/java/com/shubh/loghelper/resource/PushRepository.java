package com.shubh.loghelper.resource;

import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * A "fake" in-memory repository for Group data.
 *
 * @auhor Eric Westfall (ewestfal@gmail.com)
 */
@Repository
public class PushRepository {

    private static AtomicLong nextMemberId = new AtomicLong();

    private final HashMap<Long, String> registrationTokens = new HashMap<>();

    public String registrationToken(String registrationToken) {
        if (registrationToken == null) {
            throw new IllegalArgumentException("registrationToken was null");
        }
        registrationTokens.put(nextMemberId.getAndDecrement(), registrationToken);

        return registrationToken;
    }

    public HashMap<Long, String> getRegistrationTokens() {
        return registrationTokens;
    }
}
