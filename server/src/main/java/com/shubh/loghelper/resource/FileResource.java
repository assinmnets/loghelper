package com.shubh.loghelper.resource;

import com.google.gson.Gson;
import com.shubh.loghelper.bean.LogsMetadata;
import com.shubh.loghelper.file.FileWatchService;
import com.shubh.loghelper.mqtt.Publisher;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


@Service
@Path("/file")
@Produces(MediaType.APPLICATION_JSON)
public class FileResource {

    @Autowired
    private PushRepository repository;

    @Context
    private Response response;

    @Context
    private UriInfo uriInfo;


    @PUT()
    @Path("fileName")
    public Response updateFileName(@RequestBody String filePath) {
        FileWatchService.getInstance().setFilePath(filePath);
        return Response.ok("Updated").build();
    }

    @PUT
    @Path("logs/start")
    public Response startFileLogs(String logsMetadataString) {
        LogsMetadata logsMetadata = new Gson().fromJson(logsMetadataString,LogsMetadata.class);
        FileWatchService.getInstance().start(logsMetadata);
        return Response.ok("Started").build();
    }

    @GET
    @Path("logs/stop")
    public Response stopFileLogs() {
        FileWatchService.getInstance().stop();
        return Response.ok("Started").build();
    }

    @GET
    @Path("/logs/send/{logs}")
    public Response sendLogs(@PathParam("logs") String logs) {
        try {
            Publisher.getInstance().publish(logs);
            return Response.ok("Sent").build();
        } catch (MqttException e) {
            return Response.ok(e.getMessage()).build();
        }
    }

}
