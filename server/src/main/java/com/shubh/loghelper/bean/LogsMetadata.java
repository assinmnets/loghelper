package com.shubh.loghelper.bean;

public class LogsMetadata {
    private long sampleInterval;
    private boolean startAtBeginning;
    private String filePath;

    public long getSampleInterval() {
        return sampleInterval;
    }

    public void setSampleInterval(long sampleInterval) {
        this.sampleInterval = sampleInterval;
    }

    public boolean isStartAtBeginning() {
        return startAtBeginning;
    }

    public void setStartAtBeginning(boolean startAtBeginning) {
        this.startAtBeginning = startAtBeginning;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
