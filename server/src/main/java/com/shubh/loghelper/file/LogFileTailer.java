package com.shubh.loghelper.file;

import java.io.*;
import java.util.*;

public class LogFileTailer extends Thread {
    private long sampleInterval = 5000;

    private File logfile;

    private boolean startAtBeginning = false;

    private boolean tailing = false;

    private Set listeners = new HashSet();

    public LogFileTailer(File file) {
        this.logfile = file;
    }

    public LogFileTailer(File file, long sampleInterval, boolean startAtBeginning) {
        this.logfile = file;
        this.sampleInterval = sampleInterval;
        this.startAtBeginning = startAtBeginning;
    }

    public void addLogFileTailerListener(LogFileTailerListener l) {
        this.listeners.add(l);
    }

    public void removeLogFileTailerListener(LogFileTailerListener l) {
        this.listeners.remove(l);
    }

    protected void fireNewLogFileLine(String line) {
        for (Iterator i = this.listeners.iterator(); i.hasNext(); ) {
            LogFileTailerListener l = (LogFileTailerListener) i.next();
            l.newLogFileLine(line);
        }
    }

    public void stopTailing() {
        this.tailing = false;
    }

    public void run() {
        long filePointer = 0;

        if (this.startAtBeginning) {
            filePointer = 0;
        } else {
            filePointer = this.logfile.length();
        }

        try {
            this.tailing = true;
            RandomAccessFile file = null;
            while (this.tailing) {
                try {
                    file = new RandomAccessFile(logfile, "r");
                    long fileLength = this.logfile.length();
                    if (fileLength < filePointer) {
                        file = new RandomAccessFile(logfile, "r");
                        filePointer = 0;
                    }

                    if (fileLength > filePointer) {
                        file.seek(filePointer);
                        String line = file.readLine();
                        while (line != null) {
                            this.fireNewLogFileLine(line);
                            line = file.readLine();
                        }
                        filePointer = file.getFilePointer();
                    }

                    sleep(this.sampleInterval);
                } catch (Exception e) {
                } finally {
                    if (file != null) {
                        file.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}