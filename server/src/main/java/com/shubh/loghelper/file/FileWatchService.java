package com.shubh.loghelper.file;

import com.shubh.loghelper.bean.LogsMetadata;
import com.shubh.loghelper.mqtt.Publisher;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.File;

public class FileWatchService {

    private static final FileWatchService fileWatchService = new FileWatchService();

    private String filePath;

    private LogFileTailer logFileTailer;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public static FileWatchService getInstance() {
        return fileWatchService;
    }

    public void start(LogsMetadata logsMetadata) {
        //Path myDir = Paths.get(File.separator + "Users" + File.separator + "subhamtyagi" + File.separator + "Desktop");
        File file = new File(logsMetadata.getFilePath());
        logFileTailer = new LogFileTailer(file, logsMetadata.getSampleInterval(), logsMetadata.isStartAtBeginning());
        logFileTailer.addLogFileTailerListener(this::publishNewData);
        logFileTailer.start();
    }

    private void publishNewData(String line) {
        try {
            Publisher.getInstance().publish(line);
            System.out.println("Sent");
        } catch (MqttException e) {
            System.out.println(e.getMessage());
        }
    }

    public void stop() {
        if (logFileTailer != null) {
            logFileTailer.stopTailing();
        }
    }
}
