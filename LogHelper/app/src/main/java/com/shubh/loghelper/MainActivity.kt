package com.shubh.loghelper

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.shubh.loghelper.helpers.MqttHelper
import kotlinx.android.synthetic.main.activity_main.*
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttMessage


class MainActivity : AppCompatActivity() {
    private lateinit var mqttHelper: MqttHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startMqtt()
    }

    private fun startMqtt() {
        mqttHelper = MqttHelper(applicationContext);
        mqttHelper.setCallback(object : MqttCallbackExtended {
            override fun connectComplete(reconnect: Boolean, serverURI: String?) {
                Log.w("Debug", "Connection Complete")
            }

            override fun messageArrived(topic: String?, mqttMessage: MqttMessage?) {
                mqttMessage?.let {
                    val message = it.toString()
                    Log.w("Debug", message)
                    if (message == "FileRotated") {
                        logsTextView.text = ""
                    } else {
                        logsTextView.append(message)
                    }
                }
            }

            override fun connectionLost(cause: Throwable?) {
            }

            override fun deliveryComplete(token: IMqttDeliveryToken?) {
            }
        })
    }
}
